let colorboxes = document.getElementsByClassName("col-4");

function clearColors(){
	for(let i = 0; i < colorboxes.length; i++){
	colorboxes[i].style.background = "";
	}	
}

for(let i = 0; i < colorboxes.length; i++){
  colorboxes[i].addEventListener("click", function(){
  clearColors()
  colorboxes[i].style.background = colorboxes[i].innerHTML;
  });
}